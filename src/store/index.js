import { combineReducers } from 'redux';
import AuthReducer from './auth/auth.reducer';

export default combineReducers({
    auth: AuthReducer
})