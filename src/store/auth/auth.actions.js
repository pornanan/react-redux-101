import * as authTypes from './auth.types';

export const login = (email, password) => async dispatch => {
    dispatch({ type: authTypes.LOGIN_REQUEST })
    dispatch({ type: authTypes.LOGIN_SUCCESS, payload: { firstname: 'first' } })
}