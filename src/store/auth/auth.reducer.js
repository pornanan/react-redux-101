import * as actionTypes from './auth.types'

const initState = {
    isAuth: false,
    isAuthenticating: false,
    userInfo: {}
}

export default (state = initState, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_REQUEST: 
            return {
                ...state,
                isAuthenticating: true,
                isAuth: false,
                userInfo: {}
            }
        case actionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                isAuthenticating: false,
                isAuth: true,
                userInfo: action.payload 
            }

        default:
            return state;
    }
}
