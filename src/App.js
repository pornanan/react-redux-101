import React, { Component } from 'react';
import { connect } from 'react-redux';

import logo from './logo.svg';

import './App.css';
import TableTest from './components/Table/TableTest';
import * as authActions from './store/auth/auth.actions';

class App extends Component {

  onLogin = () => {
    this.props.login()
  }  

  render() {
    const titleTable = "";
    console.log(this.props)
    const { isAuth } = this.props.auth 
    const { userInfo } = this.props.auth

    return (
      <div className="App">
        { isAuth ? userInfo.firstname + ' are in!' : 'not login' }

        <button type="button" onClick={this.onLogin}>Login</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps, authActions)(App);
