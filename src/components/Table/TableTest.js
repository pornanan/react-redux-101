import React, { Component, Fragment } from 'react';

class TableTest extends Component {


    render(){
        const { title } = this.props

        const array = ['banana', 'apple']
        return (
            <div>
                { title ? (
                    <h4>{title}</h4>
                ) : 'No contents' }
                { array.map((item, index) => {
                    return <h4 key={index}>{item}</h4>
                } ) }
            </div>
        );
    }
}

export default TableTest;
